<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(App\User::class, 10)->create();

        $job_ids = \App\Job::pluck('id');

        foreach ($users as $user) {

            foreach ($job_ids as $job_id) {
                $user->jobs()->attach($job_id);
            }
        }
    }
}
