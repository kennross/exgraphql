<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = \App\Job::pluck('id');

        foreach ($ids as $id) {
            factory(\App\Task::class, 5)->create([
                'job_id' => $id
            ]);
        }
    }
}
