<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Ruta de ejemplo: http://exgraphql.test/graphql?query={users{id,name,email,jobs{id,name,tasks{id,name}}}}

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', function() {
    return response()->json(App\User::with('jobs.tasks')->get());
});
