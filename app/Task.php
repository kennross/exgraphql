<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
        'name',
        'description',
        'job_id'
    ];

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }
}
