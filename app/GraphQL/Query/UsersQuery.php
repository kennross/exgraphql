<?php

namespace App\GraphQL\Query;

use App\User;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;

class UsersQuery extends Query
{
    protected $attributes = [
        'name' => 'UsersQuery',
        'description' => 'A query'
    ];

    /**
     * Especificamos que las consultas a este recurso devolveran un array con listOf
     * ademá diciendole que sera nuestro tipo de dato Estudiante
     * @return GraphQL\Type\Definition\ListOfType|null
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('UserType'));
    }

    /**
     * Espeficiamos los campos y su tipo
     * @return array
     */
    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ],
            'email' => [
                'name' => 'email',
                'type' => Type::string()
            ]
        ];
    }


    /**
     * Especificamos la logica necesaria para obtener los datos del modelo
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $info
     * @return array
     */
    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        // Si recibimos el parametro "id" buscaremos en base a ese id
        if (isset($args['id'])) {
            return User::with('jobs.tasks')->where('id', $args['id'])->get();
        } else {
            // De lo contrario devolveremos la lista completa
            return User::with('jobs.tasks')->get();
        }
    }
}
