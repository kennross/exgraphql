<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class JobType extends BaseType
{
    protected $attributes = [
        'name' => 'JobType',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string()
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ],
            'tasks' => [
                'type' => Type::listOf(GraphQL::type('TaskType'))
            ]
        ];
    }
}